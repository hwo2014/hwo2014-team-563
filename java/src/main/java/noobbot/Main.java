package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import java.lang.Math;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.SerializedName;

public class Main {
	GameInit gameInit;
	
	//historic information for angle guessing
	int gameTick0;
	int pieceIndex0;
	double distanceLength0;
	double inPieceDistance0;
	double angle0;

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as "
				+ botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
				socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));
	}

	final Gson gson = new Gson();
	private PrintWriter writer;

	public Main(final BufferedReader reader, final PrintWriter writer,
			final Join join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line,
					MsgWrapper.class);

			// // 5. CAR POSITION MESSAGE
			if (msgFromServer.msgType.equals("carPositions")) {
				JsonParser parser = new JsonParser();
				JsonArray array = (JsonArray) parser.parse(line)
						.getAsJsonObject().get("data");
				int gameTick = 0;
				
				CarPositionGametick carPositionGametick = gson.fromJson(line,
						CarPositionGametick.class);
				gameTick = carPositionGametick.gameTick;
				
				// Name and key of bot
				// System.out.println("5. Server2Bot: CAR POSITION MESSAGE");
				for (int i = 0; i < array.size(); i++) {
					CarPositions carPosition = gson.fromJson(array.get(i),
							CarPositions.class);
					/*
					 * System.out.println("ID: ");
					 * System.out.println("    Name: " + carPosition.id.name);
					 * System.out.println("    Color: " + carPosition.id.color);
					 * System.out.println("Angle: " + carPosition.angle);
					 * System.out.println("Piece Position: ");
					 * System.out.println("    Piece Index: " +
					 * carPosition.piecePosition.pieceIndex);
					 * System.out.println("    In Piece Distance: " +
					 * carPosition.piecePosition.inPieceDistance);
					 * System.out.println("    Lane: ");
					 * System.out.println("        Start Lane Index: " +
					 * carPosition.piecePosition.lane.startLaneIndex);
					 * System.out.println("        End Lane Index: " +
					 * carPosition.piecePosition.lane.endLaneIndex);
					 * System.out.println("    Lap: " +
					 * carPosition.piecePosition.lap);
					 */
					double distanceLength;
					double radius = 0;
					if (Math.abs(gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].angle) > 0)
						if (gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].angle > 0) {
							radius = gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].radius - gameInit.race.track.lanes[carPosition.piecePosition.lane.startLaneIndex].distanceFromCenter;
							distanceLength = radius
									* 2
									* Math.PI
									* Math.abs(gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].angle)
									/ 360;
						}
						else {
							radius = gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].radius + gameInit.race.track.lanes[carPosition.piecePosition.lane.startLaneIndex].distanceFromCenter;
							distanceLength = radius
							* 2
							* Math.PI
							* Math.abs(gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].angle)
							/ 360;
						}
					else distanceLength = gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].length;
					System.out
							.printf("%d\t%d\t%f\t%f\t%f",
									gameTick,
									carPosition.piecePosition.pieceIndex,
									distanceLength,
									carPosition.piecePosition.inPieceDistance,
									carPosition.angle);

					switchToShortestLane(carPosition);

					// Use Throttle
					double throttle;
					if (Math.abs(gameInit.race.track.pieces[carPosition.piecePosition.pieceIndex].angle) > 0) {
						throttle = chooseThrottle(gameTick, carPosition.piecePosition.pieceIndex, distanceLength, carPosition.piecePosition.inPieceDistance, carPosition.angle, radius);
					}
					else {
						System.out.printf("\t%f\r\n",-1.0);
						throttle = 0.65;
					}
					/*
					 * if
					 * (Math.abs(gameInit.race.track.pieces[pieceIndexCircular
					 * (carPosition.piecePosition.pieceIndex + 1)].angle) > 0 ||
					 * Math.abs(gameInit.race.track.pieces[pieceIndexCircular(
					 * carPosition.piecePosition.pieceIndex + 2)].angle) > 0 ||
					 * Math.abs(gameInit.race.track.pieces[pieceIndexCircular(
					 * carPosition.piecePosition.pieceIndex + 3)].angle) > 0 ||
					 * Math.abs(gameInit.race.track.pieces[pieceIndexCircular(
					 * carPosition.piecePosition.pieceIndex + 4)].angle) > 0 ||
					 * Math.abs(gameInit.race.track.pieces[pieceIndexCircular(
					 * carPosition.piecePosition.pieceIndex + 5)].angle) > 0 ||
					 * Math.abs(gameInit.race.track.pieces[pieceIndexCircular(
					 * carPosition.piecePosition.pieceIndex + 6)].angle) > 0) {
					 * throttle = 0.65; } else throttle = 1.0;
					 */
					send(new Throttle(throttle));
					// System.out.println("7. Bot2Server: Throttle = " +
					// throttle);
				}

				// // 6. CAR CRASH MESSAGE
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("6. Server2Bot: CRASH MESSAGE");

				// // 1. JOIN MESSAGE
			} else if (msgFromServer.msgType.equals("join")) {
				JsonParser parser = new JsonParser();
				JsonElement joinMessage = parser.parse(line).getAsJsonObject()
						.get("data");
				// Name and key of bot
				Join data = gson.fromJson(joinMessage, Join.class);
				System.out.println("1. Bot2Server: JOIN MESSAGE");
				System.out.println("Name bot: " + data.name);
				System.out.println("Key bot: " + data.key);

				// // 2. YOUR CAR MESSAGE
			} else if (msgFromServer.msgType.equals("yourCar")) {
				JsonParser parser = new JsonParser();
				JsonElement yourCarMessage = parser.parse(line)
						.getAsJsonObject().get("data");
				// Name and color of bot
				YourCar data = gson.fromJson(yourCarMessage, YourCar.class);
				System.out.println("2. Server2Bot: YOUR CAR MESSAGE");
				System.out.println("Name bot: " + data.name);
				System.out.println("Color bot: " + data.color);

				// // 3. GAME INIT MESSAGE
			} else if (msgFromServer.msgType.equals("gameInit")) {
				JsonParser parser = new JsonParser();
				JsonElement gameInitData = (JsonElement) parser.parse(line)
						.getAsJsonObject().get("data");
				gameInit = gson.fromJson(gameInitData, GameInit.class);
				System.out.println("3. Server2Bot: GAME INIT MESSAGE");
				System.out.println("Race: ");
				System.out.println("    Track: ");
				System.out.println("        ID: " + gameInit.race.track.id);
				System.out.println("        Name: " + gameInit.race.track.name);
				System.out.println("        Pieces: ");
				for (int i = 0; i < gameInit.race.track.pieces.length; i++) {
					System.out.println("            Piece " + i + ":");
					System.out.println("                Length: "
							+ gameInit.race.track.pieces[i].length);
					System.out.println("                Switch: "
							+ gameInit.race.track.pieces[i].mySwitch);
					System.out.println("                Radius: "
							+ gameInit.race.track.pieces[i].radius);
					System.out.println("                Angle: "
							+ gameInit.race.track.pieces[i].angle);
				}
				System.out.println("        Lanes: ");
				for (int i = 0; i < gameInit.race.track.lanes.length; i++) {
					System.out.println("            Lane " + i + ":");
					System.out.println("                Distance from center: "
							+ gameInit.race.track.lanes[i].distanceFromCenter);
					System.out.println("                Index: "
							+ gameInit.race.track.lanes[i].index);
				}
				System.out.println("        Starting Point: ");
				System.out.println("            Position x: "
						+ gameInit.race.track.startingPoint.position.x
						+ "  y: "
						+ gameInit.race.track.startingPoint.position.y);
				System.out.println("            Angle: "
						+ gameInit.race.track.startingPoint.angle);
				System.out.println("    Cars: ");
				for (int i = 0; i < gameInit.race.cars.length; i++) {
					System.out.println("        Car " + i + ":");
					System.out.println("            ID: ");
					System.out.println("                Name: "
							+ gameInit.race.cars[i].id.name);
					System.out.println("                Color: "
							+ gameInit.race.cars[i].id.color);
					System.out.println("            Dimension: ");
					System.out.println("                Length: "
							+ gameInit.race.cars[i].dimensions.length);
					System.out.println("                Width: "
							+ gameInit.race.cars[i].dimensions.width);
					System.out
							.println("                Guide Flag Position: "
									+ gameInit.race.cars[i].dimensions.guideFlagPosition);
				}
				System.out.println("    Race Session: ");
				System.out.println("        Laps: "
						+ gameInit.race.raceSession.laps);
				System.out.println("        Max Lap Time Ms: "
						+ gameInit.race.raceSession.maxLapTimeMs);
				System.out.println("        Quick Race: "
						+ gameInit.race.raceSession.quickRace);

			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");

				// // 4. GAME START MESSAGE
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("4. Server2Bot: GAME START");
			} else {
				send(new Ping());
			}
		}
	}

	public int pieceIndexCircular(int index) {
		if (index >= gameInit.race.track.pieces.length)
			return (index - gameInit.race.track.pieces.length);
		else
			return index;
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
	
	private void switchToShortestLane(CarPositions carPosition) {
		// //Switch Lane
		int firstSwitch = 0;
		int secondSwitch = 0;
		// locate the nearest switch
		for (int j = carPosition.piecePosition.pieceIndex + 1; j < gameInit.race.track.pieces.length
				+ carPosition.piecePosition.pieceIndex; j++) {
			int k = 0;
			if (j >= gameInit.race.track.pieces.length)
				k = j - gameInit.race.track.pieces.length;
			else
				k = j;
			if (gameInit.race.track.pieces[k].mySwitch == true) {
				firstSwitch = k;
				break;
			}
		}
		// locate second nearest switch
		for (int j = firstSwitch + 1; j < gameInit.race.track.pieces.length
				+ firstSwitch; j++) {
			int k = 0;
			if (j >= gameInit.race.track.pieces.length)
				k = j - gameInit.race.track.pieces.length;
			else
				k = j;
			if (gameInit.race.track.pieces[k].mySwitch == true) {
				secondSwitch = k;
				break;
			}
		}
		double lengthLane0 = 0.0;
		double lengthLane1 = 0.0;
		// check circular switch at the end
		if (firstSwitch < secondSwitch) {
			for (int j = firstSwitch; j <= secondSwitch; j++) {
				// length of lane 0 Left Lane
				// length of lane 1 Right Lane
				double pieceLengthLane0;
				double pieceLengthLane1;
				if (gameInit.race.track.pieces[j].angle >= 0) {
					pieceLengthLane0 = gameInit.race.track.pieces[j].length
							+ Math.PI
							* (gameInit.race.track.pieces[j].radius + gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[j].angle));
					pieceLengthLane1 = gameInit.race.track.pieces[j].length
							+ Math.PI
							* (gameInit.race.track.pieces[j].radius - gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[j].angle));
				} else {
					pieceLengthLane0 = gameInit.race.track.pieces[j].length
							+ Math.PI
							* (gameInit.race.track.pieces[j].radius - gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[j].angle));
					pieceLengthLane1 = gameInit.race.track.pieces[j].length
							+ Math.PI
							* (gameInit.race.track.pieces[j].radius + gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[j].angle));
				}
				if (j == firstSwitch || j == secondSwitch) {
					pieceLengthLane0 = pieceLengthLane0 / 2;
					pieceLengthLane1 = pieceLengthLane1 / 2;
				}
				lengthLane0 = lengthLane0 + pieceLengthLane0;
				lengthLane1 = lengthLane1 + pieceLengthLane1;
			}
		} else {
			for (int j = firstSwitch; j <= firstSwitch
					+ secondSwitch; j++) {
				int k = 0;
				if (j >= gameInit.race.track.pieces.length)
					k = j - gameInit.race.track.pieces.length;
				else
					k = j;
				// length of lane 0 Left Lane
				// length of lane 1 Right Lane
				double pieceLengthLane0;
				double pieceLengthLane1;
				if (gameInit.race.track.pieces[k].angle >= 0) {
					pieceLengthLane0 = gameInit.race.track.pieces[k].length
							+ Math.PI
							* (gameInit.race.track.pieces[k].radius + gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[k].angle));
					pieceLengthLane1 = gameInit.race.track.pieces[k].length
							+ Math.PI
							* (gameInit.race.track.pieces[k].radius - gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[j].angle));
				} else {
					pieceLengthLane0 = gameInit.race.track.pieces[k].length
							+ Math.PI
							* (gameInit.race.track.pieces[k].radius - gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[k].angle));
					pieceLengthLane1 = gameInit.race.track.pieces[k].length
							+ Math.PI
							* (gameInit.race.track.pieces[k].radius + gameInit.race.track.lanes[1].distanceFromCenter
									* Math.abs(gameInit.race.track.pieces[k].angle));
				}
				if (k == firstSwitch || k == secondSwitch) {
					pieceLengthLane0 = pieceLengthLane0 / 2;
					pieceLengthLane1 = pieceLengthLane1 / 2;
				}
				lengthLane0 = lengthLane0 + pieceLengthLane0;
				lengthLane1 = lengthLane1 + pieceLengthLane1;
			}
		}

		// switch to shortest lane
		if (lengthLane0 >= lengthLane1) {
			send(new SwitchLane("Right"));
		} else
			send(new SwitchLane("Left"));
	}
	
	private double chooseThrottle(
						int gameTick,
						int pieceIndex,
						double distanceLength,
						double inPieceDistance,
						double angle,
						double radius) {
		
		//guess new car angle (gamma)
		if (gameTick > 1) {
			double v1 = inPieceDistance + distanceLength * (pieceIndex - pieceIndex0) - inPieceDistance0;
			double v2 = v1 + 0.65;
			double d_angle = 90 + angle;
			double acce_center = v1*v1 / radius;
			double v3 = Math.sqrt( acce_center*acce_center + v2*v2 - 2*acce_center*v2*Math.cos(Math.toRadians(d_angle)));
			double alpha = Math.toDegrees( Math.cosh(v1/v3*Math.sin(Math.toRadians(d_angle))) );
			double beta = v2*360/(2*Math.PI*radius);
			double gamma = alpha+beta-90;
			System.out.printf("\t%f\r\n",gamma);
		}
		//save historic info
		gameTick0 = gameTick;
		pieceIndex0 = pieceIndex;
		distanceLength0 = distanceLength;
		inPieceDistance0 = inPieceDistance;
		angle0 = angle;
		
		return 0.65;
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class JoinRace extends SendMsg {
	public final BotID botID;
	public final String trackName;
	public final int carCount;

	JoinRace(final BotID botID, final String trackName, final int carCount) {
		this.botID = botID;
		this.trackName = trackName;
		this.carCount = carCount;
	}

	class BotID {
		public final String name;
		public final String key;

		BotID(final String name, final String key) {
			this.name = name;
			this.key = key;
		}
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}

class SwitchLane extends SendMsg {
	private String value;

	public SwitchLane(String value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}

abstract class ReceiveMsg {
	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class YourCar extends ReceiveMsg {
	public final String name;
	public final String color;

	YourCar(final String name, final String color) {
		this.name = name;
		this.color = color;
	}

	@Override
	protected String msgType() {
		return "yourCar";
	}
}

class CarPositionGametick{
	public String msgType;
	public Object data;
	public String gameId;
	public int gameTick;
	CarPositionGametick(String msgType, Object data, String gameId, int gameTick){
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

class CarPositions extends ReceiveMsg {
	public ID id;
	public double angle;
	public PiecePosition piecePosition;

	CarPositions(ID id, double angle, PiecePosition piecePosition) {
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}

	class ID {
		public String name;
		public String color;

		ID(String name, String color) {
			this.name = name;
			this.color = color;
		}
	}

	class PiecePosition {
		public int pieceIndex;
		public double inPieceDistance;
		public Lane lane;
		public int lap;

		PiecePosition(int pieceIndex, double inPieceDistance, Lane lane, int lap) {
			this.pieceIndex = pieceIndex;
			this.inPieceDistance = inPieceDistance;
			this.lane = lane;
			this.lap = lap;
		}

		class Lane {
			public int startLaneIndex;
			public int endLaneIndex;

			Lane(int startLaneIndex, int endLaneIndex) {
				this.startLaneIndex = startLaneIndex;
				this.endLaneIndex = endLaneIndex;
			}
		}
	}

	protected String msgType() {
		return "carPositions";
	}
}

class GameInit extends ReceiveMsg {
	public Race race;

	GameInit(Race race) {
		this.race = race;
	}

	class Race {
		public Track track;
		public Car[] cars;
		public RaceSession raceSession;

		Race(Track track, Car[] cars, RaceSession raceSession) {
			this.track = track;
			this.cars = cars;
			this.raceSession = raceSession;
		}

		class Track {
			public String id;
			public String name;
			public Piece[] pieces;
			public Lane[] lanes;
			public StartingPoint startingPoint;

			Track(String id, String name, Piece[] pieces, Lane[] lanes,
					StartingPoint startingPoint) {
				this.id = id;
				this.name = name;
				this.pieces = pieces;
				this.lanes = lanes;
				this.startingPoint = startingPoint;
			}

			public Piece[] getPieces() {
				return pieces;
			}

			class Piece {
				public double length;
				@SerializedName("switch")
				public boolean mySwitch;
				public int radius;
				public double angle;

				Piece(double length, boolean mySwitch, int radius, double angle) {
					this.length = length;
					this.mySwitch = mySwitch;
					this.radius = radius;
					this.angle = angle;
				}
			}

			class Lane {
				public int distanceFromCenter;
				public int index;

				Lane(int distanceFromCenter, int index) {
					this.distanceFromCenter = distanceFromCenter;
					this.index = index;
				}
			}

			class StartingPoint {
				public Position position;
				public double angle;

				StartingPoint(Position position, double angle) {
					this.position = position;
					this.angle = angle;
				}

				class Position {
					public double x;
					public double y;

					Position(double x, double y) {
						this.x = x;
						this.y = y;
					}
				}
			}
		}

		class Car {
			public ID id;
			public Dimension dimensions;

			Car(ID id, Dimension dimensions) {
				this.id = id;
				this.dimensions = dimensions;
			}

			class ID {
				public String name;
				public String color;

				ID(String name, String color) {
					this.name = name;
					this.color = color;
				}
			}

			class Dimension {
				public double length;
				public double width;
				public double guideFlagPosition;

				Dimension(double length, double width, double guideFlagPosition) {
					this.length = length;
					this.width = width;
					this.guideFlagPosition = guideFlagPosition;
				}
			}
		}

		class RaceSession {
			public int laps;
			public int maxLapTimeMs;
			boolean quickRace;

			RaceSession(int laps, int maxLapTimeMs, boolean quickRace) {
				this.laps = laps;
				this.maxLapTimeMs = maxLapTimeMs;
				this.quickRace = quickRace;
			}
		}
	}

	@Override
	protected String msgType() {
		return "gameInit";
	}
}